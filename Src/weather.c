/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "weather.h"
#include "utils.h"
#include "pqws.h"
#include <string.h>

int
weather_telem_reset(weather_data_t *w)
{
  if(!w) {
    return -PQWS_INVALID_PARAM;
  }
  memset(w, 0, sizeof(weather_data_t));
  return PQWS_SUCCESS;
}

/**
 * Checks if the w is valid. The weather_telem_t is valid if all the
 * weather fields have been set using the corresponding setter functions.
 * @param w pointer to a weather_telem_t structure
 * @return 1 if weather_telem_t is valid, 0 otherwise
 */
uint8_t
weather_telem_valid(const weather_data_t *w)
{
  if(!w) {
    return 0;
  }
  return (w->valid_map == (BIT(0) | BIT(1) | BIT(2) | BIT(3) | BIT(4) | BIT(5)));
}

int
weather_update_pressure(weather_data_t *w,
                        uint16_t min, uint16_t max, uint16_t avg)
{
  if(!w) {
    return -PQWS_INVALID_PARAM;
  }

  /* Check if the field is already set */
  if(w->valid_map & BIT(0)) {
    return -PQWS_WEATHER_INFO_ALREADY_SET;
  }

  w->pressure_min = min;
  w->pressure_max = max;
  w->pressure_avg = avg;
  w->valid_map |= BIT(0);
  return PQWS_SUCCESS;
}

int
weather_update_temperature(weather_data_t *w,
                           int8_t min, int8_t max, int8_t avg)
{
  if(!w) {
    return -PQWS_INVALID_PARAM;
  }

  /* Check if the field is already set */
  if (w->valid_map & BIT(1)) {
    return -PQWS_WEATHER_INFO_ALREADY_SET;
  }

  w->temperature_min = min;
  w->temperature_max = max;
  w->temperature_avg = avg;
  w->valid_map |= BIT(1);
  return PQWS_SUCCESS;
}

int
weather_update_humidity(weather_data_t *w,
                        uint8_t min, uint8_t max, uint8_t avg)
{
  if(!w) {
    return -PQWS_INVALID_PARAM;
  }

  /* Check if the field is already set */
  if(w->valid_map & BIT(2)) {
    return -PQWS_WEATHER_INFO_ALREADY_SET;
  }

  w->humidity_min = min;
  w->humidity_max = max;
  w->humidity_avg = avg;
  w->valid_map |= BIT(2);
  return PQWS_SUCCESS;
}

int
weather_update_mq7(weather_data_t *w,
                   uint16_t min, uint16_t max, uint16_t avg)
{
  if(!w) {
    return -PQWS_INVALID_PARAM;
  }

  /* Check if the field is already set */
  if(w->valid_map & BIT(3)) {
    return -PQWS_WEATHER_INFO_ALREADY_SET;
  }

  w->mq7_min = min;
  w->mq7_max = max;
  w->mq7_avg = avg;
  w->valid_map |= BIT(3);
  return PQWS_SUCCESS;
}

int
weather_update_mq131(weather_data_t *w,
                     uint16_t min, uint16_t max, uint16_t avg)
{
  if(!w) {
    return -PQWS_INVALID_PARAM;
  }

  /* Check if the field is already set */
  if(w->valid_map & BIT(4)) {
    return -PQWS_WEATHER_INFO_ALREADY_SET;
  }

  w->mq131_min = min;
  w->mq131_max = max;
  w->mq131_avg = avg;
  w->valid_map |= BIT(4);
  return PQWS_SUCCESS;
}

int
weather_update_mics(weather_data_t *w,
                    uint16_t min, uint16_t max, uint16_t avg)
{
  if(!w) {
    return -PQWS_INVALID_PARAM;
  }

  /* Check if the field is already set */
  if(w->valid_map & BIT(5)) {
    return -PQWS_WEATHER_INFO_ALREADY_SET;
  }

  w->mics_min = min;
  w->mics_max = max;
  w->mics_avg = avg;
  w->valid_map |= BIT(5);
  return PQWS_SUCCESS;
}


