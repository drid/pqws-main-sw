/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "serial_cosnole.h"
#include "atmo.h"
#include <stdio.h>
#include <string.h>
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
//  uint8_t command[8];
//  HAL_UART_Receive_IT(huart, command, sizeof(command));
//
//}
extern UART_HandleTypeDef huart2;
extern RTC_HandleTypeDef hrtc;
extern RTC_DateTypeDef sDate;
extern RTC_TimeTypeDef sTime;
extern uint8_t getAtmo;
uint8_t serialCommandExec (uint8_t* line)
{
  uint16_t power, y, m, d, h, min, s;
  uint8_t cmd[10];
  uint8_t dateTimeStr[22] =
    { 0 };
  if (strncmp ((char*)line, "rtcset", 6) == 0) {
    sscanf ((char*)line, "%s 20%hu-%hu-%huT%hu:%hu:%hu" ,cmd , &y, &m, &d, &h, &min, &s);
//    sscanf ((char*)line, "%s 20%hhu-%hhu-%hhuT%hhu:%hhu:%hhu" ,cmd , &sDate.Year, &sDate.Month,
//                    &sDate.Date, &sTime.Hours, &sTime.Minutes, &sTime.Seconds);
    sDate.WeekDay=RTC_WEEKDAY_TUESDAY;
    sDate.Year=y;
    sDate.Month=m;
    sDate.Date=d;
    sTime.Hours=h;
    sTime.Minutes=min;
    sTime.Seconds=s;
  }
  else {
    sscanf ((char*)line, "%s %hhu\r", cmd, &power);
  }
  if (strcmp ("start", (char*)cmd) == 0) {
    getAtmo=1;
    HAL_UART_Transmit (&huart2, (uint8_t*) UART_START_MESSAGE,
                               sizeof(UART_START_MESSAGE), 1000);
  } else if (strcmp ("stop", (char*)cmd) == 0) {
    getAtmo=0;
    HAL_UART_Transmit (&huart2, (uint8_t*) UART_STOP_MESSAGE,
                           sizeof(UART_STOP_MESSAGE), 1000);
  } else if (strcmp ("fan", (char*)cmd) == 0) {
    atmoSetFanPower ((uint8_t) power);
    HAL_UART_Transmit (&huart2, (uint8_t*) UART_OK_MESSAGE,
                       sizeof(UART_OK_MESSAGE), 1000);
  }
  else if (strcmp ("mq7", (char*)cmd) == 0) {
    atmoSetHeaterPower (PQ_ATMO_MQ7, (uint8_t) power);
    HAL_UART_Transmit (&huart2, (uint8_t*) UART_OK_MESSAGE,
                       sizeof(UART_OK_MESSAGE), 1000);
  }
  else if (strcmp ("mq131", (char*)cmd) == 0) {
    atmoSetHeaterPower (PQ_ATMO_MQ131, (uint8_t) power);
    HAL_UART_Transmit (&huart2, (uint8_t*) UART_OK_MESSAGE,
                       sizeof(UART_OK_MESSAGE), 1000);
  }
  else if (strcmp ("mics", (char*)cmd) == 0) {
    atmoSetHeaterPower (PQ_ATMO_MICS, (uint8_t) power);
    HAL_UART_Transmit (&huart2, (uint8_t*) UART_OK_MESSAGE,
                       sizeof(UART_OK_MESSAGE), 1000);
  }
  else if (strcmp ("off", (char*)cmd) == 0) {
    atmoSetFanPower ((uint8_t) 0);
    atmoSetHeaterPower (PQ_ATMO_MICS, (uint8_t) 0);
    atmoSetHeaterPower (PQ_ATMO_MQ131, (uint8_t) 0);
    atmoSetHeaterPower (PQ_ATMO_MQ7, (uint8_t) 0);
    HAL_UART_Transmit (&huart2, (uint8_t*) UART_OK_MESSAGE,
                       sizeof(UART_OK_MESSAGE), 1000);
  }
  else if (strcmp ("rtcset", (char*)cmd) == 0) {
    sTime.TimeFormat     = RTC_HOURFORMAT_24;
    sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE ;
    sTime.StoreOperation = RTC_STOREOPERATION_RESET;
    HAL_RTC_SetDate (&hrtc, &sDate, RTC_FORMAT_BIN);
    HAL_RTC_SetTime (&hrtc, &sTime, RTC_FORMAT_BIN);
    HAL_UART_Transmit (&huart2, (uint8_t*) UART_OK_MESSAGE,
                       sizeof(UART_OK_MESSAGE), 1000);
  }
  else if (strcmp ("rtc", (char*)cmd) == 0) {
    HAL_RTC_GetTime (&hrtc, &sTime, RTC_FORMAT_BIN);
    HAL_RTC_GetDate (&hrtc, &sDate, RTC_FORMAT_BIN);
    sprintf ((char*) dateTimeStr, "%u-%02u-%02uT%02u:%02u:%02u\r\n",
             2000 + sDate.Year, sDate.Month, sDate.Date, sTime.Hours,
             sTime.Minutes, sTime.Seconds);
    HAL_UART_Transmit (&huart2, (uint8_t*) dateTimeStr, sizeof(dateTimeStr),
                       1000);
  }
  else if (strcmp ("read", (char*)cmd) == 0) {
    atmoRequestData();
    HAL_UART_Transmit (&huart2, (uint8_t*) UART_OK_MESSAGE,
                           sizeof(UART_OK_MESSAGE), 1000);
  } else {
    HAL_UART_Transmit (&huart2, (uint8_t*) UART_WTF_MESSAGE,
                       sizeof(UART_WTF_MESSAGE), 1000);
  }
  return 0;
}
