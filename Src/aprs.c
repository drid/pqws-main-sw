/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2018 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aprs.h"
#include "pqws.h"
#include "stm32l4xx_hal.h"
#include <string.h>

extern RTC_HandleTypeDef hrtc;

static char __aprs_report[256];

int
aprs_init(aprs_conf_t *conf,
          const uint8_t *dest_addr,
          uint8_t dest_ssid,
          const uint8_t *src_addr,
          uint8_t src_ssid,
          aprs_type_t type,
          const char *lat,
          const char *lon)
{
  int ret;

  if(!conf || !lon || !lat) {
    return -PQWS_INVALID_PARAM;
  }

  /* Sanity checks for lon/lat coordinates */
  if(strnlen(lon, APRS_LON_STR_LEN) != APRS_LON_STR_LEN
      || strnlen(lat, APRS_LAT_STR_LEN) != APRS_LAT_STR_LEN) {
    return -PQWS_INVALID_PARAM;
  }

  memcpy(conf->lon, lon, APRS_LON_STR_LEN);
  memcpy(conf->lat, lat, APRS_LAT_STR_LEN);

  ret = ax25_init(&conf->hax25, dest_addr, dest_ssid, src_addr, src_ssid,
                  AX25_PREAMBLE_LEN, AX25_POSTAMBLE_LEN,
                  AX25_UI_FRAME, 0x3, 0xF0);
  if(ret) {
    return ret;
  }

  return PQWS_SUCCESS;
}


/**
 * Get an APRS compatible (DHM) weather timestamp in UTC
 * @param out the string to hold the timestamp. The string should be
 * at least 8 bytes according to the APRS specification (7 digits + null termination).
 * The resulting string is a valid null terminated string.
 * @return
 */
static int
aprs_get_weather_timestamp(char *out)
{
  int ret;
  RTC_DateTypeDef date;
  RTC_TimeTypeDef time;
  if(!out) {
    return -PQWS_INVALID_PARAM;
  }
  ret = HAL_RTC_GetTime(&hrtc, &time, RTC_FORMAT_BIN);
  if(ret) {
    return ret;
  }
  ret = HAL_RTC_GetDate(&hrtc, &date, RTC_FORMAT_BIN);
  if(ret) {
    return ret;
  }
  sprintf(out, "%02u", date.Date);
  sprintf(out + 2, "%02u", time.Hours);
  sprintf(out + 4, "%02u", time.Minutes);
  out[6] = 'z';
  out[7] = 0;
  return PQWS_SUCCESS;
}


int
aprs_tx_weather_report(aprs_conf_t *conf, ax5043_conf_t *hax5043,
                       const weather_data_t *w)
{
  int ret;
  uint32_t report_len = 0;
  char timestamp[8];

  if(!conf || !w || !weather_telem_valid(w)) {
    return -PQWS_INVALID_PARAM;
  }

  ret = aprs_get_weather_timestamp(timestamp);
  if(ret) {
    return ret;
  }
  __aprs_report[report_len++] = APRS_WTHR_COMPLETE_RPRT_SYMBOL;
  memcpy(__aprs_report + report_len, timestamp, APRS_WTHR_COMPLETE_RPRT_TM_LEN);
  report_len += APRS_WTHR_COMPLETE_RPRT_TM_LEN;
  memcpy(__aprs_report + report_len, conf->lat, APRS_LAT_STR_LEN);
  report_len += APRS_LAT_STR_LEN;
  __aprs_report[report_len++] = APRS_WTHR_SYMBOL_ID;
  memcpy(__aprs_report + report_len, conf->lon, APRS_LON_STR_LEN);
  report_len += APRS_LON_STR_LEN;
  __aprs_report[report_len++] = APRS_WTHR_SYMBOL_CODE;
  memcpy(__aprs_report + report_len, APRS_NO_WIND_INFO_STR, APRS_WIND_STR_LEN);
  report_len += APRS_WIND_STR_LEN;
  /* We do not have gust information */
  __aprs_report[report_len++] = 'g';
  __aprs_report[report_len++] = '.';
  __aprs_report[report_len++] = '.';
  __aprs_report[report_len++] = '.';

  /* Report temperature */
  sprintf(__aprs_report + report_len, "t%03d", w->temperature_avg);
  report_len += 4;

  /* Report humidity */
  if(w->humidity_avg > 99) {
    sprintf(__aprs_report + report_len, "h%02u", 0);
  }
  else{
    sprintf(__aprs_report + report_len, "h%02u", w->humidity_avg);
  }
  report_len += 3;

  /* Report pressure */
  sprintf(__aprs_report + report_len, "b%05u", w->pressure_avg);
  report_len += 6;

  __aprs_report[report_len++] = APRS_SW_ID;
  memcpy(__aprs_report + report_len, APRS_WX_UNIT_STR, APRS_WX_UNIT_STR_LEN);
  report_len += APRS_WX_UNIT_STR_LEN;

  /* Extra sensors information is placed on the comment section */
  ret = sprintf(__aprs_report + report_len, "t%03d:%03d:%03d", w->temperature_min,
                w->temperature_max, w->temperature_avg);
  if(ret < 1) {
    return -PQWS_INVALID_PARAM;
  }
  report_len += (uint32_t)ret;

  ret = sprintf(__aprs_report + report_len, "h%02u:%02u:%02u", w->humidity_min,
                w->humidity_max, w->humidity_avg);
  if(ret < 1) {
    return -PQWS_INVALID_PARAM;
  }
  report_len += (uint32_t)ret;

  ret = sprintf(__aprs_report + report_len, "b%05u:%05u:%05u", w->pressure_min,
                w->pressure_max, w->pressure_avg);
  if(ret < 1) {
    return -PQWS_INVALID_PARAM;
  }
  report_len += (uint32_t)ret;

  ret = sprintf(__aprs_report + report_len, "mq7%05u:%05u:%05u", w->mq7_min,
                w->mq7_max, w->mq7_avg);
  if(ret < 1) {
    return -PQWS_INVALID_PARAM;
  }
  report_len += (uint32_t)ret;

  ret = sprintf(__aprs_report + report_len, "mq131%05u:%05u:%05u", w->mq131_min,
                w->mq131_max, w->mq131_avg);
  if (ret < 1) {
    return -PQWS_INVALID_PARAM;
  }
  report_len += (uint32_t) ret;

  ret = sprintf(__aprs_report + report_len, "mics%05u:%05u:%05u", w->mics_min,
                w->mics_max, w->mics_avg);
  if (ret < 1) {
    return -PQWS_INVALID_PARAM;
  }
  report_len += (uint32_t) ret;

  return ax25_tx_frame(&conf->hax25, hax5043, __aprs_report, report_len);
}

