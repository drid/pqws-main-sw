/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PQWS_H_
#define PQWS_H_

#include <stdint.h>
#include "ax5043.h"
#include "aprs.h"
#include "weather.h"

typedef struct {
  uint32_t sysTick;
  float battVolt;
  float inputVolt;
} pqws_system_status_t;

/**
 * Error codes of the PQWS. Users should return the negative of the error
 * codes. NO_ERROR is set to zero.
 */
typedef enum {
  PQWS_SUCCESS = 0,                    //!< All ok!
  PQWS_INVALID_PARAM,                   //!< An invalid parameter was given
  PQWS_MAX_SPI_TRANSFER_ERROR,          //!< The requested SPI data transfer was larger than supported
  PQWS_TIMEOUT,                         //!< A timeout occurred
  PQWS_WEATHER_INFO_ALREADY_SET         //!< A weather information field has been already set
} pqws_error_t;

typedef struct
{
  ax5043_conf_t         hax5043;
  aprs_conf_t           haprs;
} pqws_rf_conf_t;

int
pqws_rf_init(pqws_rf_conf_t *conf, SPI_HandleTypeDef *hspi);

int
pqws_tx_weather_aprs(pqws_rf_conf_t *conf, const weather_data_t *w);


#endif /* PQWS_H_ */
