/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2018 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APRS_H_
#define APRS_H_

#include "ax25.h"
#include "ax5043.h"
#include "weather.h"

#define APRS_WTHR_COMPLETE_RPRT_SYMBOL  '@'
#define APRS_WTHR_COMPLETE_RPRT_TM_LEN  (7)
#define APRS_WTHR_SYMBOL_ID             '/'
#define APRS_WTHR_SYMBOL_CODE           '_'
#define APRS_LON_STR_LEN                (9)
#define APRS_LAT_STR_LEN                (8)
#define APRS_WIND_STR_LEN               (7)
#define APRS_SW_ID                      'X'
#define APRS_WX_UNIT_STR                "PQWS"
#define APRS_WX_UNIT_STR_LEN            (4)


#define APRS_NO_WIND_INFO_STR           ".../..."

typedef enum
{
  APRS_WEATHER = 0
} aprs_type_t;

typedef struct
{
  aprs_type_t   type;
  ax25_conf_t   hax25;
  char          lon[APRS_LON_STR_LEN];
  char          lat[APRS_LAT_STR_LEN];
} aprs_conf_t;

int
aprs_init(aprs_conf_t *conf,
          const uint8_t *dest_addr,
          uint8_t dest_ssid,
          const uint8_t *src_addr,
          uint8_t src_ssid,
          aprs_type_t type,
          const char *lat,
          const char *lon);

int
aprs_tx_weather_report(aprs_conf_t *conf, ax5043_conf_t *hax5043,
                       const weather_data_t *w);

#endif /* APRS_H_ */
