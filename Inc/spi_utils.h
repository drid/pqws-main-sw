/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPI_UTILS_H_
#define SPI_UTILS_H_

#include <stdint.h>


#define SPI_MAX_TRASNFER_SIZE           512
#define SPI_TIMEOUT                     10000

/**
 * Enumeration of all the available SPI devices
 */
typedef enum {
  SPI_DEV_AX5043 = 0,           //!< AX5043 RF chip
  SPI_DEV_NUM                   //!< Total number of SPI devices
} spi_dev_t;


int
spi_sel_dev(spi_dev_t dev, uint8_t enable);



#endif /* SPI_UTILS_H_ */
