/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SERIAL_COSNOLE_H_
#define SERIAL_COSNOLE_H_
#include "stm32l4xx_hal.h"

#define UART_RX_BUFFER_SIZE     1

#define UART_INIT_MESSAGE       "\n\r PQWS Station (v0.9.1)\n\n\r ©2018 Libre Space Foundation\n\n\r Firmware 0.9a\n\n\rReady\n\r"
#define UART_ERROR_MESSAGE      "\r\nERROR\n\r"
#define UART_OK_MESSAGE         "\r\nOK\n\r"
#define UART_START_MESSAGE      "\r\nAtmo started\n\r"
#define UART_STOP_MESSAGE       "\r\nAtmo stopped\n\r"
#define UART_WTF_MESSAGE        "\r\nUnknown command\n\r"
uint8_t serialCommandExec(uint8_t *RxBuffer);
#endif /* SERIAL_COSNOLE_H_ */
